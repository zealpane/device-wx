import Vue from 'vue'
import Router from 'vue-router'
import Home from '../pages/user/Login'
import lab from '../pages/lab/index'
import equip from '../pages/equip/index'
import index from '../pages/index'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: index,
      children: [
        {
          path: '/Mycenter',
          component: function (resolve) {
            require(['../pages/user/Mycenter.vue'], resolve)
          }
        }, {
          path: '/',
          component: equip
        }, {
          path: '/lab',
          component: lab
        }, {
          path: '/HelloFromVux',
          component: function (resolve) {
            require(['../components/HelloFromVux.vue'], resolve)
          }
        }, {
          path: '/myinfo',
          component: function (resolve) {
            require(['../pages/user/myinfo.vue'], resolve)
          }
        }, {
          path: '/updatepwd',
          component: function (resolve) {
            require(['../pages/user/updatepwd.vue'], resolve)
          }
        }, {
          path: '/question',
          component: function (resolve) {
            require(['../pages/user/question.vue'], resolve)
          }
        }, {
          path: '/teacherAppointment',
          component: resolve => {
            require(['../pages/user/teacherAppointment.vue'], resolve)
          }
        }, {
          path: '/teacherAppointDetail/:id(\\d+)',
          component: resolve => {
            require(['../pages/user/teacherAppointDetail.vue'], resolve)
          }
        }, {
          path: '/studentAppointment',
          component: resolve => {
            require(['../pages/user/studentAppointment.vue'], resolve)
          }
        }, {
          path: '/Myapply',
          component: function (resolve) {
            require(['../pages/lab/lab.vue'], resolve)
          }
        }, {
          path: '/equipList',
          component: function (resolve) {
            require(['../pages/apply/equipList.vue'], resolve)
          }
        }, {
          name: '/applyequip',
          path: '/applyequip',
          component: function (resolve) {
            require(['../pages/apply/applyequip.vue'], resolve)
          }
        }, {
          path: '/Labapply',
          component: function (resolve) {
            require(['../pages/equip/Labapply.vue'], resolve)
          }
        }, {
          path: '/Equipdetail',
          component: function (resolve) {
            require(['../pages/equip/Equipdetail.vue'], resolve)
          }
        }, {
          path: '/Labdetail',
          component: function (resolve) {
            require(['../pages/lab/Labdetail.vue'], resolve)
          }
        }
      ]
    }, {
      path: '/register',
      component: function (resolve) {
        require(['../pages/user/register.vue'], resolve)
      }
    }, {
      path: '/login',
      component: Home
    }, {
      path: '/equipList',
      component: function (resolve) {
        require(['../pages/apply/equipList.vue'], resolve)
      }
    }, {
      path: '/applyequip',
      component: function (resolve) {
        require(['../pages/apply/applyequip.vue'], resolve)
      }
    }
  ]
})
