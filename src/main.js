import Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import App from './App'
import router from './router/index.js'
import axios from 'axios'
import {ConfigPlugin} from 'vux'
import Vuex from 'vuex'
Vue.use(Vuex)
Vue.use(ConfigPlugin, {
  $layout: 'VIEW_BOX'
})
// Vue.use(AjaxPlugin)
Vue.use(VueRouter)

FastClick.attach(document.body)

Vue.config.productionTip = false
Vue.http = Vue.prototype.$http = axios
Vue.http.interceptors.response.use(
   response => {
     console.log(response)

     if (response.data.code === 1) {
       alert('未登录，请登录！')
       router.push('/login')
     } else {
       return response
     }
   },
  error => {
    console.log(error.response)
  }
)
/* eslint-disable no-new */
const store = new Vuex.Store({}) // 这里你可能已经有其他 module

store.registerModule('vux', { // 名字自己定义
  state: {
    isLoading: false,
    islist: false,
    equiplist: [],
    nums: []
  },
  mutations: {
    updateLoadingStatus (state, payload) {
      state.isLoading = payload.isLoading
    },
    SAVEPARAS (state, data) {
      state.equiplist.push(data)
    },
    REMOVEPARAS (state, data) {
      state.equiplist.splice(data, 1)
    },
    SETLIST (state, data) {
      state.equiplist = data
      // router.push('/applyequip')
    },
    SPLICE (state, data) {
      state.equiplist.splice(data, 1)
    },
    SETNUMBER (state, data) {
      state.nums.push(data)
    },
    REMOVENUMBER (state, data) {
      state.nums.splice(data, 1)
    }
  },
  actions: {
    svaeParams ({commit}, data) {
      commit('SAVEPARAS', data)
    },
    removeParams ({commit}, data) {
      commit('REMOVEPARAS', data)
    },
    setnumber ({commit}, data) {
      commit('SETNUMBER', data)
    },
    removenumber ({commit}, data) {
      commit('REMOVENUMBER', data)
    },
    setlist ({commit}, data) {
      commit('SETLIST', data)
    },
    splice ({commit}, data) {
      commit('SPLICE', data)
    }
  }
})
router.beforeEach(function (to, from, next) {
  store.commit('updateLoadingStatus', {isLoading: true})
  next()
})

router.afterEach(function (to) {
  store.commit('updateLoadingStatus', {isLoading: false})
})
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app-box')
