@echo off
title GIT提交批处理：这一段代码同样可以在类unix平台运行

echo 开始提交代码到本地仓库
echo 当前目录是：%cd%

echo 开始添加变更
git add *

echo;
echo 开始提交变更到本地仓库
set /p message=输入修改:
git commit -m "%message%"

echo;
echo 开始拉取
git pull

echo;
echo 开始推送到远程服务器
git push origin master

echo;
echo 批处理执行完毕！