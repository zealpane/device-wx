# device

> device management
>
> 此项目使用`vux`组件库进行开发，然而此项目如今已不再维护。[官网链接](https://vux.li/)已不可用

后端源码地址：[springbootFramework (gitee.com)](https://gitee.com/gdata/springbootFramework)

* 这一版中还未前后端分离开发

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
